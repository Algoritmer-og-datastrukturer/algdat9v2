import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Filewriter  {

    private static Graph g;
    Node node;

    /**
     * For korte ruter, skriver hver node til fil
     * @param test
     * @throws IOException
     */
    static public  void write2file(ArrayList<Node>test) throws IOException {
        FileWriter writer = new FileWriter("src\\funnetRuteKort.csv");
        BufferedWriter bw = new BufferedWriter(writer);
        bw.write("Nr"+","+"Latitude"+","+"longitude");
        for (Node node : test) {
            bw.newLine();
            bw.write(test.indexOf(node)+","+node.lat + "," + node.longitude);
        }
       bw.close();
       writer.close();
    }

    /**
     * For lange ruter, skriver annenhver node til fil
     * @param test
     * @throws IOException
     */
    static public  void write2fileStor(ArrayList<Node>test) throws IOException {
        FileWriter writer = new FileWriter("src\\funnetRuteLang.csv");
        BufferedWriter bw = new BufferedWriter(writer);
        bw.write("Nr"+","+"Latitude"+","+"longitude");
        for (Node node :test) {
            if(test.indexOf(node)%2==0){
                continue;
            }
            bw.newLine();
            bw.write(test.indexOf(node)+","+node.lat + "," + node.longitude);
        }
        bw.close();
        writer.close();

    }



}
