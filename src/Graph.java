import java.io.*;
import java.util.ArrayList;
import java.util.PriorityQueue;

public class Graph {

    private LinkedEdges edges;
    private int noNodes;
    private Node[] nodes;


    static ArrayList<Node> Dijkstra;
    static ArrayList<Node> ALTalgo;
    static ArrayList<Node> pathsToCodes;





    public Graph(File edgeFile, File nodeFile, File typeFile) throws IOException {
        BufferedReader nodeReader = new BufferedReader(new FileReader(nodeFile.getPath()));
        BufferedReader edgeReader = new BufferedReader(new FileReader(edgeFile.getPath()));
        BufferedReader typeReader = new BufferedReader(new FileReader(typeFile.getPath()));

        noNodes = Integer.parseInt(nodeReader.readLine().trim());
        edgeReader.readLine();

        nodes = new Node[noNodes];
        edges = new LinkedEdges(noNodes);

        String[] edgeTokens = new String[10];

        String line;
        //leser kantene fra fil
        while((line = edgeReader.readLine()) != null){
            splitLine(line, 3, edgeTokens);
            edges.pushBack(Integer.parseInt(edgeTokens[0]),
                    new Edge(Integer.parseInt(edgeTokens[0]), Integer.parseInt(edgeTokens[1]),
                            Integer.parseInt(edgeTokens[2]),
                            null));
        }
        edgeReader.close();

        //leser nodene fra fil
        while((line = nodeReader.readLine()) != null){
            //Hopp over innledende blanke, finn starten på ordet
            splitLine(line, 3 , edgeTokens);
            nodes[Integer.parseInt(edgeTokens[0])] = new Node(Integer.parseInt(edgeTokens[0]),
                    Double.parseDouble(edgeTokens[1]),
                    Double.parseDouble(edgeTokens[2]));
        }
        nodeReader.close();

        typeReader.readLine();
        //leser type noder fra fil
        while((line = typeReader.readLine()) != null){
            //Hopp over innledende blanke, finn starten på ordetwhile
            splitLine(line, 2 , edgeTokens);
            nodes[Integer.parseInt(edgeTokens[0])].type = Integer.parseInt(edgeTokens[1]);
        }
        nodeReader.close();
    }

    private void splitLine(String line, int numWords, String[] results) {
        int j = 0;
        int lengde = line.length();
        for (int i = 0; i < numWords; ++i) {
            while (line.charAt(j) <= ' ') ++j;
            int ordstart = j;//Finn slutten på ordet, hopp over ikke-blanke
            while (j < lengde && line.charAt(j) > ' ') ++j;
            results[i] = line.substring(ordstart, j);
        }
    }

    public ArrayList<Node> dijkstraToStation(int start, int typeGoal, int numToFind){
        resetNodes();
        System.out.println("Dijkstra begynner søk etter nærmeste bensin/ladestasjoner");
        System.out.println("nærmeste stajsoner til node "+ start);
        long startTime = System.currentTimeMillis();

        int leftToFind = numToFind;
        ArrayList<Node>[] results = new ArrayList[numToFind];
        ArrayList<Node> sisteNoden = new ArrayList();
        PriorityQueue<Node> pq = new PriorityQueue<>();
        nodes[start].distTo = 0;
        pq.add(nodes[start]);
        int counter = 0;
        Node current;
        //mens det finnes kanter fra den nåværende noden til andre noder
        while(!pq.isEmpty()) {
            counter++;
            current = pq.poll();
            //sjekker om det er noen nye noder/kortere veier og oppdaterer distansene
            checkConnectedNodes(pq, current);

            //sjekker om noden vi er i nå er passende type
            if(current.type == typeGoal && !current.found){
                current.found = true;
                //legger til den lenkede listen med dette resultatet i arrayet vårt

                results[numToFind - leftToFind] = shortestPathLinkedList(start, current.index);
                sisteNoden.add(current);
                leftToFind--;
            }

            //dersom vi har funnet alle
            if(leftToFind == 0){
                System.out.println("Dijkstra fant " + numToFind + " stasjoner, brukte " + (System.currentTimeMillis() - startTime) + " Millisekunder");
                System.out.println("Behandlet " + counter + " noder");
                return sisteNoden;
            }
        }
        return sisteNoden;
    }

    /**
     * denne brukes i alle metodene til å sjekke kantene til en gitt node
     * sjekker om en gitt node har kanter til noder som ikke er funnet, eller om de har kortere
     * veier til noen kortere noder
     *
     */
    private void checkConnectedNodes(PriorityQueue<Node> pq, Node current) {
        if(edges.getHead(current.index) != null)  {
            for (Edge edge : edges.getHead(current.index)) {
                Node nextNode = nodes[edge.getTo()];
                //dersom noden ikke har blitt funnet før
                if (nextNode.distTo == Integer.MAX_VALUE / 2) {
                    //find distance returnerer 0 dersom goal er null
                    nextNode.distTo = current.distToTime + edge.getWeight() ;
                    nextNode.distToTime = current.distToTime + edge.getWeight();
                    pq.add(nextNode);
                    nextNode.pastNode = current.index;
                }
                //dersom noden vi er i nå har en kortere vei til noden som kanten peker på
                else if (current.distToTime + edge.getWeight() < nextNode.distToTime) {
                    nextNode.distTo = current.distToTime + edge.getWeight()  ;
                    nextNode.distToTime = current.distToTime + edge.getWeight();
                    nextNode.pastNode = current.index;
                    //oppdaterer nodene slik i tilfelle noden som vi akkurat oppdaterte nå skal
                    // ligge øverst
                    Node top = pq.poll();
                    if(top != null) pq.add(top);
                }
            }
        }
    }

    private void resetNodes(){
        for (Node node : nodes) {
            node.distTo = Integer.MAX_VALUE / 2;
        }
    }

    public ArrayList<Node> shortestPath(int start, int goal){
        resetNodes();
        //noden som vi bruker til å estimere reisetid, med null så blir ikke denne estimert

        System.out.println("Dijkstra leter etter korteste vei ");

        long startTime = System.currentTimeMillis();

        PriorityQueue<Node> pq = new PriorityQueue<>();
        nodes[start].distTo = 0;
        nodes[start].distToTime = 0;
        pq.add(nodes[start]);
        Node current;
        //mens det finnes kanter fra den nåværende noden til andre noder
        int counter = 0;
        while(!pq.isEmpty()) {
            current = pq.poll();
            counter++;
            if(current.index == goal){
                System.out.println("Dijkstra fant korteste vei " +
                        "behandlet " + counter + " noder");
                //gjør om nanosekunder til millisekunder
                System.out.println("Brukte: " + (System.currentTimeMillis() - startTime) + " " +
                        "millisekunder" );
                return shortestPathLinkedList(start,goal);
            }
            checkConnectedNodes(pq,current);
        }
        System.out.println("Fant ingen vei ");
        return null;
    }

    /**
     * lager en lenket liste med korteste vei ved å bruke node-arrayet
     */
    private ArrayList<Node> shortestPathLinkedList(int start, int to){
        Node current = nodes[to];
        ArrayList<Node> result = new ArrayList<>();
        result.add(current);
        while(current.index != start){
            current = nodes[current.pastNode];
            Node newNode = new Node(current.index,current.lat,current.longitude);
            newNode.distTo = current.distTo;
            newNode.distToTime = current.distToTime;
            result.add(newNode);
        }

        return result;
    }
    private int findDistance(Node currentNode, Node goal){
        if(goal == null) return 0;
        double currentBreddegrad = currentNode.lat * Math.PI / 180;
        double currentLengdegrad = currentNode.longitude * Math.PI / 180;

        double goalBreddegrad = goal.lat * Math.PI / 180;
        double goalLengdegrad = goal.longitude * Math.PI / 180;

        int EARTH_RADIUS = 6371;

        double distance = 2 * EARTH_RADIUS *
                Math.asin(
                        Math.sqrt(
                                (Math.sin( ((currentBreddegrad - goalBreddegrad) / 2 )
                                        * Math.sin( (currentBreddegrad - goalBreddegrad) / 2 ))

                                        + ( ( Math.cos(currentBreddegrad) )
                                        * ( Math.cos(goalBreddegrad) )
                                        * ( Math.sin( (currentLengdegrad - goalLengdegrad) / 2 ) )
                                        * ( Math.sin( (currentLengdegrad - goalLengdegrad) / 2 ) )
                                )
                                )
                        ));

        return (int) (distance * 360000/130);
    }





    public static void main(String[] args) throws IOException {

        //koder:  1= stedsnavn,2 bensinstasjoner,4 ladestasjoner
        int fromNode = 136963 ;
        int toNode =  6861306;
        int stedNode = 3587110 ;
        int stationType = 2;
        int numStations = 10;

        Dijkstra = new ArrayList<>();
        ALTalgo = new ArrayList<>();

                                    //endre pathname etter hvor filene ligger så klart.
        Graph graph = new Graph(new File("Resources/kantListe.txt"),new File("Resources/nodeListe.txt"),new File("Resources/interessepunktListe.txt"));

        //djikstra fra til
       Dijkstra = graph.shortestPath(fromNode, toNode);
        System.out.println("Total reisetid med dijkstra er " + (Dijkstra.get(0).distToTime/100)/3600 +
                " Timer, " + ((((Dijkstra.get(0).distToTime)/100)%3600)/60) + " Minutter " +
                "og " + ((Dijkstra.get(0).distToTime/100)%60) + " sekunder");
        Filewriter.write2fileStor(Dijkstra);

        //finne n antall nærmeste stasjoner
        pathsToCodes = graph.dijkstraToStation(stedNode, stationType, numStations);
        Filewriter.write2file(pathsToCodes);



    }
}
